//
//  MenuViewController.swift
//  SlideMenuExample
//
//  Created by rafiul hasan on 10/29/19.
//  Copyright © 2019 rafiul hasan. All rights reserved.
//

import UIKit

protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index: Int32)
}

class MenuViewController: UIViewController {

    var buttonMenu: UIButton!
    var menudelegate: SlideMenuDelegate?
    
    @IBOutlet weak var menuOverlay: UIButton!
    
    // Array to display menu options
    @IBOutlet var menuOptionsTableView : UITableView!
    
    //Array containing menu options
    var menuOptionsArray = [Dictionary<String,String>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuOptionsTableView.delegate = self
        menuOptionsTableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateMenuOptions()
    }
    
    func updateMenuOptions(){
        menuOptionsArray.append(["title":"Home", "icon":"HomeIcon"])
        menuOptionsArray.append(["title":"Setting", "icon":"PlayIcon"])
        menuOptionsArray.append(["title":"Profile", "icon":"PlayIcon"])
        menuOptionsArray.append(["title":"Friends", "icon":"PlayIcon"])
        menuOptionsArray.append(["title":"Followers", "icon":"PlayIcon"])
        menuOptionsArray.append(["title":"Following", "icon":"PlayIcon"])
        menuOptionsArray.append(["title":"Privacy Setting", "icon":"PlayIcon"])
        menuOptionsArray.append(["title":"logout", "icon":"PlayIcon"])
        
        menuOptionsTableView.reloadData()
    }
    
    @IBAction func buttonMenuClose(_ sender: UIButton) {
        buttonMenu.tag = 0
        buttonMenu.isHidden = false
        if self.menudelegate != nil {
            var index = Int32(sender.tag)
            if sender == self.menuOverlay {
                index = -1
            }
            menudelegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParent()
        })
    }

}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuOptionsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.layoutMargins = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clear
                
        //cell.image = UIImage(named: menuOptionsArray[indexPath.row]["icon"]!)
        cell.textLabel!.text = menuOptionsArray[indexPath.row]["title"]!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let btn = UIButton(type: UIButton.ButtonType.custom)
        btn.tag = indexPath.row
        self.buttonMenuClose(btn)
    }
    
}
